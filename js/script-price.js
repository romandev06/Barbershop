// Бургер меню

const icons = document.querySelectorAll('.burger-icon');
icons.forEach (icon => {  
    icon.addEventListener('click', () => {
        icon.classList.toggle("open");
    });
});



//  открытие бургер меню

const burgerIcon = document.querySelector('.burger-icon')
const headerNav = document.querySelector('.header-nav')

burgerIcon.addEventListener('click', function() {
    headerNav.classList.toggle('header-nav__hidden')
})



// клавиша Escape работает только, когда бургер меню открыто

window.addEventListener('keydown', (event) => {
    if (!headerNav.classList.contains('header-nav__hidden') && (event.target === headerNav ||  event.key === 'Escape')) {
        headerNav.classList.add('header-nav__hidden')
        icons.forEach(icon => {
            icon.classList.toggle('open')
        })
    }
})


// progress line

let wrapper = document.querySelector('.wrapper')
let progressLine = document.querySelector('.progress-line')

window.addEventListener('scroll', function() {
    let progressBar = window.scrollY / (wrapper.clientHeight - window.innerHeight) * 100

    progressLine.style.width = progressBar + '%'
})