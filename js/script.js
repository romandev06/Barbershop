// Свайпер

const swiper = new Swiper(".mySwiper", {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});





// Модальное окно (галерея изображений)

const myProjects = document.querySelectorAll('.swiper-slide__project img')
const myProjectsGallery = document.querySelector('.my-projects__article')
const myProjectImg = document.querySelector('.project-img')
const closeMyProjects = document.querySelector('.cross-icon__close')


myProjects.forEach(project => project.addEventListener('click', function() {
    myProjectsGallery.style.display = 'flex'

    myProjectImg.src = project.getAttribute('src')
}))





// закрытие модального окна (галереи изображений)

closeMyProjects.addEventListener('click', function() {
    myProjectsGallery.style.display = 'none'
})

window.addEventListener('keydown', function(event) {
    if (event.key === 'Escape') {
        myProjectsGallery.style.display = 'none'
    }
})





// Бургер меню

const icons = document.querySelectorAll('.burger-icon');
icons.forEach (icon => {  
    icon.addEventListener('click', () => {
        icon.classList.toggle("open");
    });
});



//  открытие бургер меню

const burgerIcon = document.querySelector('.burger-icon')
const headerNav = document.querySelector('.header-nav')

burgerIcon.addEventListener('click', function() {
    headerNav.classList.toggle('header-nav__hidden')
})



// клавиша Escape работает только, когда бургер меню открыто

window.addEventListener('keydown', (event) => {
    if (!headerNav.classList.contains('header-nav__hidden') && (event.target === headerNav ||  event.key === 'Escape')) {
        headerNav.classList.add('header-nav__hidden')
        icons.forEach(icon => {
            icon.classList.toggle('open')
        })
    }
})



// progress line

let wrapper = document.querySelector('.wrapper')
let progressLine = document.querySelector('.progress-line')

window.addEventListener('scroll', function() {
    let progressBar = window.scrollY / (wrapper.clientHeight - window.innerHeight) * 100

    progressLine.style.width = progressBar + '%'
})